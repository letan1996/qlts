const express = require("express");
const restRouter = express.Router();

const signInRouter = require("./SignIn/router");
const userRouter = require("./Users/router");
const uploadRouter = require("./Upload/router");
const phongBanRouter = require("./PhongBan/router");
const nhomTaiSanRouter = require("./NhomTaiSan/router");
const trangThaiRouter = require("./TrangThai/router");
const nhaCungCapRouter = require("./NhaCungCap/router");
const doiTuongSuDungRouter = require("./DoiTuongSuDung/router");
const taiSanRouter = require("./TaiSan/router");
const phieuBaoDuongRouter = require("./PhieuBaoDuong/router");

restRouter.use("/auth", signInRouter);
restRouter.use("/user", userRouter);
restRouter.use("/upload", uploadRouter);
restRouter.use("/phong-ban", phongBanRouter);
restRouter.use("/nhom-tai-san", nhomTaiSanRouter);
restRouter.use("/trang-thai", trangThaiRouter);
restRouter.use("/nha-cung-cap", nhaCungCapRouter);
restRouter.use("/doi-tuong-su-dung", doiTuongSuDungRouter);
restRouter.use("/tai-san", taiSanRouter);
restRouter.use("/phieu-bao-duong", phieuBaoDuongRouter);

module.exports = restRouter;