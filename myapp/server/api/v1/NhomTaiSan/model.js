const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    tenNhomTaiSan: {
      type: String,
      required: true
    },
    maNhomTaiSan: {
      type: String
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("nhomtaisans", schema);
