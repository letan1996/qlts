const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    ngayDieuChuyen: {
      type: Date
    },
    idTaiSan: {
      type: Schema.ObjectId,
      ref: 'taisans'
    },
    idPhongBan: {
      type: Schema.ObjectId,
      ref: 'phongbans'
    },
    idDoiTuongSuDung: {
      type: Schema.ObjectId,
      ref: 'doituongsudungs'
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("phieudieuchuyens", schema);
