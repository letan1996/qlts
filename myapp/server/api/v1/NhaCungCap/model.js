const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    tenNhaCungCap: {
      type: String,
      required: true
    },
    diaChi: {
      type: String
    },
    sdt: {
      type: String
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("nhacungcaps", schema);
