const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    ngayBaoDuong: {
      type: Date
    },
    kinhPhi: {
      type: Number
    },
    idTaiSan: {
      type: Schema.ObjectId,
      ref: 'taisans'
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("phieubaoduongs", schema);
