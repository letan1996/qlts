const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    tenTrangThai: {
      type: String,
      required: true
    }
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("trangthais", schema);
