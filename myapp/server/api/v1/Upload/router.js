const express = require("express");
const uploadController = require("./controller");
const passport = require('passport');
const upload = require("./service");
const router = express.Router();

router.post('/multiple',
  passport.authenticate('jwt', { session: false }),
  upload.array('file', 12),
  uploadController.uploadMultipleFiles
)

router.post('/single',
  passport.authenticate('jwt', { session: false }),
  upload.single('file', 12),
  uploadController.uploadSingleFile
)

module.exports = router;