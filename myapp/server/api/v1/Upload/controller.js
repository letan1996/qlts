
module.exports = {
  uploadSingleFile: async (req, res) => {
    try {
      const { file } = req;
      if (file) {
        const item = {
          linkFile: `upload/images/${file.filename}`,
          fileName: file.originalname
        }
        res.status(200).json({ data: item });
      } else {
        return res.status(403).json({
          msg: 'No Data'
        });
      }
    } catch (error) {
      return res.status(500).json({
        msg: error
      });
    }
  },
  uploadMultipleFiles: async (req, res) => {
    try {
      if (req.files.length > 0) {
        const arrFile = req.files.map((file) => {
          const item = {
            linkFile: `upload/images/${file.filename}`,
            fileName: file.originalname
          }
          return item;
        });
        res.status(200).json({ data: arrFile });
      } else {
        return res.status(403).json({
          msg: 'No Data'
        });
      }
    } catch (error) {
      return res.status(500).json({
        msg: error
      });
    }
  }
}