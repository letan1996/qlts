const multer = require('multer');
const path = require('path');
const ACCEPT_FILE_LIST = [
  'image/jpeg',
  'image/png',
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
];
const LIMIT_FILE_SIZE = 25; //MB
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'server/public/upload/images');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  }
});
const fileFilter = (req, file, cb) => {
  const isMatch = ACCEPT_FILE_LIST.some(type => type === file.mimetype);
  if (isMatch) {
    cb(null, true);
  } else {
    cb(null, false);
  }
}
const limits = {
  fileSize: LIMIT_FILE_SIZE * 1000000
}
const upload = multer({ storage, fileFilter, limits })
module.exports = upload;