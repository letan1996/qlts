const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    idPhongBan: {
      type: Schema.ObjectId,
      ref: 'phongbans'
    },
    idNhaCungCap: {
      type: Schema.ObjectId,
      ref: 'nhacungcaps'
    },
    idNhomTaiSan: {
      type: Schema.ObjectId,
      ref: 'nhomtaisans'
    },
    idTrangThai: {
      type: Schema.ObjectId,
      ref: 'trangthais'
    },
    maTaiSan: {
      type: String
    },
    tenTaiSan: {
      type: String
    },
    giaTri: {
      type: Number
    },
    thoiGianBaoHanh: {
      type: Number
    },
    thongSoKyThuat: {
      type: String
    },
    soLuong: {
      type: Number
    },
    ngayMua: {
      type: Date
    },
    ngayBatDauSuDung: {
      type: Date
    },
    moTa: {
      type: String
    },
    mucDich: {
      type: String
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("taisans", schema);
