const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    hoTen: {
      type: String,
      required: true
    },
    idPhongBan: {
      type: Schema.ObjectId,
      ref: 'phongbans'
    },
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("doituongsudungs", schema);
