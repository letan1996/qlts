const User = require("./model");
const userService = require("./service");
const messages = require("../../../constants/messages");

module.exports = {
  async create(req, res) {
    try {
      const { password, username, ...rest } = req.body;
      const encryptedPass = userService.encryptPassword(password);
      const data = await User.find({ username });
      if (data.length > 0) {
        return res.status(400).json({
          msg: messages.DATA_IS_EXSISTED
        });
      }
      const newData = new User({ ...rest, username, password: encryptedPass });
      newData.save((error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({
          msg: messages.SUCCESS,
          data: doc
        });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async update(req, res) {
    try {
      const { password, ...reqBody } = req.body;
      const reqParams = req.params;
      if (password) {
        reqBody.password = await userService.encryptPassword(password);
      }
      const data = await User.find({
        $and: [
          { _id: { $ne: reqParams.id } },
          { username: reqBody.username },
        ]
      });
      if (data.length > 0) {
        return res.status(400).json({
          msg: messages.DATA_IS_EXSISTED
        });
      }
      User.findByIdAndUpdate(reqParams.id, reqBody, { new: true }, (error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({ data: doc });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async updateToken(req, res) {
    try {
      const { tokenThietBi } = req.body;
      const reqUser = req.user;
      const data = await User.find({
        $and: [
          { _id: reqUser._id }
        ]
      });
      if (data.length < 0) {
        return res.status(400).json({
          msg: messages.NOT_FOUND
        });
      }
      User.findByIdAndUpdate(reqUser._id, { tokenThietBi }, { new: true }, (error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({ data: doc });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async delete(req, res) {
    try {
      const reqParams = req.params;
      User.findOneAndRemove({ _id: reqParams.id }, (error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({ data: doc });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async getAll(req, res) {
    try {
      const reqQuery = req.query;
      const pageSize = parseInt(reqQuery.pageSize) || 10;
      const page = parseInt(reqQuery.page) || 1;
      let conditions = {};
      const { keyword } = reqQuery;
      if (keyword) {
        conditions = {
          $or: [
            { username: keyword ? { $regex: keyword, $options: 'i' } : { $exists: true } },
            { fullName: keyword ? { $regex: keyword, $options: 'i' } : { $exists: true } }
          ]
        }
      }
      const content = page && pageSize
        ? User.find(conditions).sort({ _id: -1 }).skip((page - 1) * pageSize).limit(pageSize)
        : User.find(conditions).sort({ _id: -1 })
      const [data, totalDocs] = await Promise.all([
        content,
        User.find(conditions).countDocuments()
      ]);
      const totalPage = Math.ceil(totalDocs / pageSize);
      const response = {
        docs: data,
        totalDocs,
        pageSize,
        totalPage,
        page
      }
      return res.status(200).json({ data: response });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async getOne(req, res) {
    try {
      const reqParams = req.params;
      User.findOne({ _id: reqParams.id }, (error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({ data: doc });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  authenticate(req, res) {
    const reqParams = req.params;
    return res.status(200)({
      data: { ...reqParams.user }
    });
  }
}