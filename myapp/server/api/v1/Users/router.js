const express = require("express");
const controller = require("./controller");
const passport = require('passport');
const router = express.Router();

router.route("/")
  .get(passport.authenticate('jwt', { session: false }), controller.getAll)
  .post(passport.authenticate('jwt', { session: false }), controller.create)
  .put(passport.authenticate('jwt', { session: false }), controller.updateToken)
router.route("/me")
  .get(passport.authenticate('jwt', { session: false }), controller.authenticate)
router.route("/:id")
  .get(passport.authenticate('jwt', { session: false }), controller.getOne)
  .put(passport.authenticate('jwt', { session: false }), controller.update)
  .delete(passport.authenticate('jwt', { session: false }), controller.delete)

module.exports = router;