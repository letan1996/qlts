const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
      lowercase: true
    },
    fullName: {
      type: String,
      default: 'No name'
    },
    password: {
      type: String,
      required: true
    },
    tokenDevice: {
      web: { type: String },
      ios: { type: String },
      android: { type: String }
    },
    anhThe: {
      type: String
    },
    refreshToken: {
      type: String
    },
    isXoa: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: { createdAt: 'created_at' } }
);

module.exports = mongoose.model("users", schema);
