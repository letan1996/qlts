const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    tenPhongBan: {
      type: String,
      unique: true,
      required: true
    }
  },
  { timestamps: { createdAt: 'created_at' } }
);
module.exports = mongoose.model("phongbans", schema);
