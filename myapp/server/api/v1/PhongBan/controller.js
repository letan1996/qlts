const Model = require("./model");
const messages = require("../../../constants/messages");

module.exports = {
  async create(req, res) {
    try {
      const reqBody = req.body;
      const newData = new Model(reqBody);
      newData.save((error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({
          msg: messages.SUCCESS,
          data: doc
        });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async update(req, res) {
    try {
      const reqBody = req.body;
      const reqParams = req.params;
      Model.findByIdAndUpdate(reqParams.id, reqBody, { new: true }, (error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({ data: doc });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async delete(req, res) {
    try {
      const reqParams = req.params;
      Model.findOneAndRemove({ _id: reqParams.id }, (error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({ data: doc });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async getAll(req, res) {
    try {
      const reqQuery = req.query;
      const pageSize = parseInt(reqQuery.pageSize);
      const page = parseInt(reqQuery.page);
      const { keyword } = reqQuery;
      const conditions = {
        $or: [
          { tenPhongBan: keyword ? { $regex: keyword, $options: 'i' } : { $exists: true } },
        ]
      }
      const [data, totalDocs] = await Promise.all([
        page && pageSize
          ? Model.find(conditions).sort({ _id: -1 }).skip((page - 1) * pageSize).limit(pageSize)
          : Model.find(conditions).sort({ _id: -1 }),
        Model.find(conditions).countDocuments()
      ]);
      const totalPage = Math.ceil(totalDocs / pageSize);
      const response = {
        docs: data,
        totalDocs,
        pageSize,
        totalPage,
        page
      }
      return res.status(200).json({ data: response });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  },
  async getOne(req, res) {
    try {
      const reqParams = req.params;
      Model.findOne({ _id: reqParams.id }, (error, doc) => {
        if (error) {
          throw error;
        }
        return res.status(200).json({ data: doc });
      });
    } catch (error) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${error}`
      });
    }
  }
}