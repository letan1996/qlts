const User = require("../Users/model");
const messages = require("../../../constants/messages");
const jwt = require("../../../helpers/jwt");
const userService = require("../Users/service");
module.exports = {
  login(req, res) {
    try {
      const { username, password } = req.body;
      User.findOne({ username }, async (err, doc) => {
        if (err) {
          return res.status(500).json({
            msg: `${messages.SEVER_ERROR}:${err}`
          });
        }
        if (!doc) {
          return res.status(404).json({
            msg: messages.FAIL
          });
        } else {
          const authenticated = await userService.comparePassword(password, doc.password);
          if (!authenticated) {
            return res.status(403).json({ msg: messages.FAIL });
          }
          const token = jwt.createToken({ id: doc._id });
          const refreshToken = jwt.createRefreshToken({ id: doc._id });
          doc.refreshToken = refreshToken;
          await doc.save((err) => {
            if (err) {
              return res.status(500).json({
                msg: `${messages.SEVER_ERROR}:${err}`
              });
            }
            const { fullName, username, anhThe } = doc;
            const response = {
              token,
              refreshToken,
              user: { fullName, username, anhThe }
            }
            return res.status(200).json({ data: response });
          });
        }
      });
    } catch (err) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${err}`
      });
    }
  },
  async token(req, res) {
    try {
      const reqBody = req.body;
      const payload = await jwt.verifyRefreshToken(reqBody.refreshToken);
      const data = await User.findById(payload.id);
      if (!data) {
        return res.status(404).json({
          msg: messages.NOT_FOUND
        });
      }
      const refreshToken = jwt.createRefreshToken({ id: data.id });
      const token = jwt.createToken({ id: data.id });
      data.refreshToken = refreshToken;
      const response = {
        token
      }
      data.save((err) => {
        if (err) {
          return res.status(500).json({
            msg: `${messages.SEVER_ERROR}:${err}`
          });
        }
        res.status(200).json(response);
      });
    } catch (err) {
      return res.status(500).json({
        msg: `${messages.SEVER_ERROR}:${err}`
      });
    }
  }
}