const express = require("express");
const controller = require("./controller");
const router = express.Router();

router.route("/login")
  .post(controller.login);
router.route("/token")
  .post(controller.token);

module.exports = router;