const express = require("express");
const restRouter = express.Router();

const v1 = require('./v1');

restRouter.use("/v1", v1);

module.exports = restRouter;