module.exports = {
  DATA_IS_EXSISTED: 'DATA_IS_EXSISTED',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL',
  SEVER_ERROR: 'SEVER_ERROR',
  NOT_FOUND: 'NOT_FOUND',
  TITLE_CONG_VIEC: 'Bạn có 1 công việc cần hoàn thành'
}