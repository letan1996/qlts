const mongoose = require("mongoose");
const { getConfig } = require("./config");
const userService = require('../api/v1/Users/service');
const User = require('../api/v1/Users/model');
mongoose.Promise = global.Promise;
const config = getConfig("production");
mongoose.Promise = global.Promise;
const options = {
  user: 'adminqlcv',
  pass: 'qlcv@123@',
  auth: { authSource: "qlcv" },
  useNewUrlParser: true,
  socketTimeoutMS: 0,
  keepAlive: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
};
module.exports = function () {
  mongoose.connect(config.MONGO_URI, options);
  mongoose.connection.on("error", err => {
    console.log("err", err)
  })
  mongoose.connection.on("connected", async () => {
    const userAdmin = {
      username: 'admin',
      password: '1'
    }
    const { password, username } = userAdmin;
    const encryptedPass = userService.encryptPassword(password);
    const data = await User.find({ username });
    if (data.length > 0) {
      console.log('User đã tồn tại')
    } else {
      const newData = new User({ username, password: encryptedPass });
      newData.save();
    }
  })
  mongoose.connection.on('disconnected', function () {
    console.log("Mongoose default connection is disconnected");
  });
  process.on('SIGINT', function () {
    mongoose.connection.close(function () {
      console.log(termination("Mongoose default connection is disconnected due to application termination"));
      process.exit(0)
    });
  });
}