const config = {
  production: {
    secret: '$QTI123',
    MONGO_URI: "mongodb://localhost:27017/qlts",
    port: 4000,
    tokenSecret: '$QTI123',
    tokenLife: 60 * 60 * 60,
    refreshTokenLife: 60 * 60 * 60,
    refreshTokenSecret: "$QTI123"
  },
  development: {
    MONGO_URI: "mongodb://localhost:27017/qlts",
    port: 4000,
    tokenSecret: '$QTI123',
    tokenLife: 60 * 60 * 60,
    refreshTokenLife: 60 * 60 * 60 * 60,
    refreshTokenSecret: "$QTI123",
  }
};

module.exports.getConfig = env => config[env] || config.development;
