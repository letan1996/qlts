const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser')
const cors = require("cors");
const passport = require('passport');
const configJWTStrategy = require('./middlewares/passport-jwt');
const app = express();
const connect = require("./config/db");
const indexRouter = require('./api/index');

connect();

app.use(logger('dev'));
app.use(cors());
app.options(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './public')));
configJWTStrategy();
app.use(passport.initialize());
app.use('/api', indexRouter);
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.message = "Invalid route";
  error.status = 404;
  next(error);
});
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  return res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
