/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

const firebaseConfig = {
  apiKey: "AIzaSyC7EZpjTdyGftFCbhTqILmegCtKLa2po1Y",
  authDomain: "teee-bac06.firebaseapp.com",
  projectId: "teee-bac06",
  storageBucket: "teee-bac06.appspot.com",
  messagingSenderId: "185917232160",
  appId: "1:185917232160:web:ca897c0b70724a1f579845",
  measurementId: "G-HDZ5K7G3C1"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.onBackgroundMessage(payload => {
  const { data } = payload;
  const value = JSON.parse(data.body);
  const url = `https://qlcv.webquangnam.com/cong-viec/quan-ly-cong-viec/chinh-sua/${value.id}`;
  const options = {
    body: value.body,
    icon: "/logo.png",
    data: { url }
  }
  return self.registration.showNotification(data.title, options);
});
self.addEventListener('notificationclick', function (event) {
  event.waitUntil(async function () {
    if (event.notification.data.url) {
      await clients.openWindow(event.notification.data.url);
    }
  }());
});