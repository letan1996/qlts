const { messaging } = require('./firebaseInit');

const sendNotificationToClient = (message) => {
  // Send a message to the devices corresponding to the provided
  // registration tokens.
  messaging
    .sendMulticast(message)
    .then(response => {
      // Response is an object of the form { responses: [] }
      const successes = response.responses.filter(r => r.success === true)
        .length;
      const failures = response.responses.filter(r => r.success === false)
        .length;
      console.log(
        'Notifications sent:',
        `${successes} successful, ${failures} failed`
      );
    })
    .catch(error => {
      console.log('Error sending message:', error);
    });
}

const sendToDevice = (message) => {
  // Send a message to the devices corresponding to the provided
  // registration tokens.
  messaging
    .send(message)
    .then((response) => {
      console.log(`Notifications sent:${response}`);
    })
    .catch(error => {
      console.log('Error sending message:', error);
    });
}

module.exports = {
  sendNotificationToClient,
  sendToDevice
}