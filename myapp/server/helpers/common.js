const { map } = require("lodash");
const moment = require('moment');
module.exports = {
  convert: (arr, page, pageSize) => {
    const newData = map(arr, (item, index) => {
      return { key: page === 1 ? index + 1 : index + ((page - 1) * pageSize) + 1, ...item };
    });
    return newData;
  },
  convertDateToTimeCronJob: (value, HOURS) => {
    const timeCronJob = {
      dayOfWeek: '*',
      hours: '*',
      minutes: '*'
    }
    const date = moment(value).subtract({ hours: 3 });
    timeCronJob.dayOfWeek = date.day();
    timeCronJob.month = date.month() + 1;
    timeCronJob.dayOfMonth = date.date();
    timeCronJob.hours = date.hour();
    timeCronJob.minutes = date.minute();
    timeCronJob.millisecond = date.milliseconds();
    return timeCronJob;
  }
}