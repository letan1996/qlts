const jwt = require("jsonwebtoken");
const { getConfig } = require("../config/config");

const config = getConfig(process.env.NODE_ENV);
module.exports = {
  createToken(payload) {
    return jwt.sign(payload, config.tokenSecret, {
      expiresIn: config.tokenLife
    });
  },
  createRefreshToken(payload) {
    return jwt.sign(payload, config.refreshTokenSecret, {
      expiresIn: config.refreshTokenLife
    });
  },
  verifyRefreshToken(payload) {
    return jwt.verify(payload, config.refreshTokenSecret);
  }
};
