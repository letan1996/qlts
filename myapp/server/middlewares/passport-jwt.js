const Passport = require("passport");
const PassportJWT = require("passport-jwt");
const { getConfig } = require("../config/config");
const User = require("../api/v1/Users/model");

const config = getConfig(process.env.NODE_ENV);
module.exports = configJWTStrategy = () => {
  const opts = {
    jwtFromRequest: PassportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.tokenSecret
  };
  Passport.use(
    new PassportJWT.Strategy(opts, (payload, done) => {
      User.findOne({ _id: payload.id }, (err, user) => {
        if (err) {
          return done(err);
        }
        if (user) {
          return done(null, user);
        }
        return done(null, false);
      });
    })
  );
};
