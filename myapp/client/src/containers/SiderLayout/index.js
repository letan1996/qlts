import { Layout, Menu } from 'antd';
import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { KEY_HE_THONG, KEY_THONG_KE } from 'src/constants/actionTypes';
import { listNavItemHeThong, listNavItemThongKe, listNavItemTrangChu } from 'src/containers/listNav';

const { Sider } = Layout;

export default function SiderLayout({ collapsed }) {
	const [listNav, setListNav] = useState(listNavItemTrangChu);
	const [keyNav, setKeyNav] = useState('');
	const location = useLocation();
	const pathname = location.pathname;
	const history = useHistory();

	useEffect(() => {
		switch (keyNav) {
			case KEY_HE_THONG:
				setListNav(listNavItemHeThong)
				break;
			case KEY_THONG_KE:
				setListNav(listNavItemThongKe)
				break;
			default:
				setListNav(listNavItemTrangChu);
				break;
		}
	}, [keyNav])

	useEffect(() => {
		switch (true) {
			case pathname.includes(KEY_HE_THONG):
				setKeyNav(KEY_HE_THONG);
				break;
			case pathname.includes(KEY_THONG_KE):
				setKeyNav(KEY_THONG_KE);
				break;
			default:
				setKeyNav('');
				break;
		}
	}, [pathname])

	const onClickMenu = (values) => {
		history.push(values.key);
	}

	const renderSelectKey = () => {
		const pathname = location.pathname;
		if (pathname === '/') {
			return '/trang-chu';
		}
		return pathname;
	}

	return (
		<Sider className="layout__sider" trigger={null} collapsible collapsed={collapsed}>
			<div className="layout__sider__logo" />
			<Menu theme="dark" mode="inline" selectedKeys={[renderSelectKey()]} onClick={onClickMenu}>
				{listNav.map((item, index) => {
					return (
						<Menu.Item key={item.key} icon={item.icon}>
							{item.title}
						</Menu.Item>
					)
				})}
			</Menu>
		</Sider>
	)
}
