import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import MainApp from './MainApp';
import SignIn from '../SignIn/container';
import PrivateRoute from 'src/guards/PrivateRoute';
export default function App() {
	const { loggedIn } = useSelector(state => state.auth);
	const match = useRouteMatch();
	return (
		<Switch>
			<Route path="/login" component={SignIn} exact />
			<PrivateRoute loggedIn={loggedIn} component={MainApp} path={match.url} />
		</Switch>
	);
}
