import { Layout } from 'antd';
import React, { useState } from 'react';
import App from 'src/routes';
import HeaderLayout from '../HeaderLayout';
import SiderLayout from '../SiderLayout';

const { Content } = Layout;

export default function MainAp() {
    const [collapsed, setCollapsed] = useState(false);
    const toggle = () => {
        setCollapsed(prevState => !prevState);
    }

    return (
        <Layout className="layout__wrap">
            <HeaderLayout collapsed={collapsed} toggle={toggle} />
            <Layout>
                <SiderLayout collapsed={collapsed} />
                <Layout>
                    <Content>
                        <App />
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    )
}
