import {
    DashboardOutlined, DollarCircleOutlined, GroupOutlined, HomeOutlined, PieChartOutlined, UsergroupAddOutlined, UserOutlined
} from '@ant-design/icons';
import React from 'react';

const DashBoard = React.lazy(() => import('src/routes/NghiepVu/DashBoard'));
const NguoiDung = React.lazy(() => import('src/routes/HeThong/NguoiDung'));
const TaiSan = React.lazy(() => import('src/routes/NghiepVu/TaiSan'));
const PhongBan = React.lazy(() => import('src/routes/NghiepVu/PhongBan'));
const ThongKeTheoNam = React.lazy(() => import('src/routes/ThongKe/ThongKeTheoNam'));

export const listNavItemTrangChu = [
    {
        key: '/trang-chu', icon: <DashboardOutlined />,
        title: 'Trang chủ',
        Cmp: DashBoard,
        exact: true
    },
    {
        key: '/tai-san',
        icon: <DollarCircleOutlined />,
        title: 'Tài sản',
        Cmp: TaiSan,
        exact: true
    },
    {
        key: '/nha-cung-cap',
        icon: <HomeOutlined />,
        title: 'Nhà cung cấp',
        exact: true
    },
    {
        key: '/nhom-tai-san',
        icon: <GroupOutlined />,
        title: 'Nhóm tài sản',
        exact: true
    },
    {
        key: '/phong-ban',
        icon: <HomeOutlined />,
        title: 'Phòng ban',
        Cmp: PhongBan,
        exact: true
    },
    {
        key: '/doi-tuong-su-dung',
        icon: <UserOutlined />,
        title: 'Đối tượng sử dụng',
        exact: true
    }
]
export const listNavItemThongKe = [
    {
        key: '/thong-ke/theo-nam',
        icon: <PieChartOutlined />,
        title: 'Thống kê theo năm',
        Cmp: ThongKeTheoNam,
        exact: true
    }
]
export const listNavItemHeThong = [
    {
        key: '/he-thong/nguoi-dung',
        icon: <UsergroupAddOutlined />,
        title: 'Người dùng',
        Cmp: NguoiDung,
        exact: true
    }
]