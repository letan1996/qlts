import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Image, Input } from 'antd';
import React from 'react';
import { useHistory } from 'react-router-dom';

const SignIn = ({ loginRequest }) => {
    const history = useHistory();
    const onFinish = values => {
        loginRequest({ ...values, history });
    };

    return (
        <div className="login">
            <div className="login-wrap">
                <div className="login-banner">
                    <Image alt="boohoo" src={require('../../assets/images/banner/p1.jpg')} />
                </div>
                <Form
                    className="login-form"
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Username!',
                            },
                        ]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Password!',
                            },
                        ]}
                    >
                        <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="Password"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Form.Item name="remember" valuePropName="checked" noStyle>
                            <Checkbox>Remember me</Checkbox>
                        </Form.Item>
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Log in
                </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
};

export default SignIn;