import SignIn from './index';
import { connect } from 'react-redux';
import { loginRequest } from 'src/appRedux/actions/auth'
const mapStateToProps = state => ({
    loggedIn: state.auth.loggedIn
});

const mapDispatchToProps = { loginRequest };
export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
