import {
  DashboardOutlined,
  PieChartOutlined, SettingOutlined
} from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { KEY_HE_THONG, KEY_THONG_KE } from 'src/constants/actionTypes';
import { listNavItemHeThong, listNavItemThongKe, listNavItemTrangChu } from 'src/containers/listNav';

const { Header } = Layout;
const listNavItem = [
  { key: '/', icon: <DashboardOutlined />, title: 'Trang chủ' },
  { key: KEY_HE_THONG, icon: <SettingOutlined />, title: 'Hệ thống' },
  { key: KEY_THONG_KE, icon: <PieChartOutlined />, title: 'Thống kê' }
]
export default function HeaderLayout() {
  const location = useLocation();
  const history = useHistory();

  const onClickMenu = (values) => {
    const key = values.key;
    let location;
    switch (key) {
      case KEY_HE_THONG:
        location = listNavItemHeThong[0].key;
        break;
      case KEY_THONG_KE:
        location = listNavItemThongKe[0].key;
        break;
      default:
        location = listNavItemTrangChu[0].key
        break;
    }
    history.push(location);
  }

  const renderKey = () => {
    const pathname = location.pathname;
    if (pathname.includes(KEY_HE_THONG)) {
      return KEY_HE_THONG;
    } else if (pathname.includes(KEY_THONG_KE)) {
      return KEY_THONG_KE;
    } else {
      return '/';
    }
  }
  return (
    <Header className="layout__header">
      {/* {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'layout__header__trigger',
        onClick: toggle,
      })} */}
      <div className="layout__header__logo" />
      <Menu theme="dark" mode="horizontal" selectedKeys={renderKey()} onClick={onClickMenu}>
        {listNavItem.map((item, index) => {
          return (
            <Menu.Item key={item.key}>{item.title}</Menu.Item>
          )
        })}
      </Menu>
    </Header>
  )
}
