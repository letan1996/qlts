import { useEffect, useReducer, useState } from "react";
import { axiosClient } from "src/api/node_modules/src/helpers";
import axios from 'axios';

const actionTypes = {
	FETCH_INIT: 'FETCH_INIT',
	FETCH_SUCCESS: 'FETCH_SUCCESS',
	FETCH_FAILURE: 'FETCH_FAILURE'
}
const dataFetchReducer = (state, action) => {
	switch (action.type) {
		case actionTypes.FETCH_INIT:
			return {
				...state,
				isLoading: true,
				isError: false
			};
		case actionTypes.FETCH_SUCCESS:
			return {
				...state,
				isLoading: false,
				isError: false,
				content: action.payload
			};
		case actionTypes.FETCH_FAILURE:
			return {
				...state,
				isLoading: false,
				isError: true,
				error: action.error
			};
		default:
			throw new Error();
	}
};

const useFetchApi = (initialUrl, initialData) => {
	const [url, setUrl] = useState(initialUrl);
	const [state, dispatch] = useReducer(dataFetchReducer, {
		isLoading: false,
		isError: false,
		content: initialData
	});

	useEffect(() => {
		const cancelToken = axios.CancelToken;
		const source = cancelToken.source();
		const fetchData = async () => {
			const options = {
				url,
				method: 'GET',
				cancelToken: source.token
			}
			dispatch({ type: actionTypes.FETCH_INIT });
			try {
				const result = await axiosClient(options);
				dispatch({ type: actionTypes.FETCH_SUCCESS, payload: result });
			} catch (error) {
				dispatch({ type: actionTypes.FETCH_FAILURE, error });
			}
		};
		fetchData();
		return () => {
			source.cancel("axios request cancelled");
		};
	}, [url]);
	return [state, setUrl];
};
export default useFetchApi;