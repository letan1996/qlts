import React from 'react';
import MainApp from 'src/containers/App';
import { BrowserRouter as Router, Route } from 'react-router-dom'

export default function App() {
  return (
    <Router>
      <Route path="/">
        <MainApp />
      </Route>
    </Router>
  );
}
