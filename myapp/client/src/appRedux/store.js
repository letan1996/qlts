import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducers";
import rootSaga from './sagas';

const history = createBrowserHistory();

export default function configureStore(preloadedState) {
    const sagaMiddleware = createSagaMiddleware();
    const routeMiddleware = routerMiddleware(history);
    const middleware = [sagaMiddleware, routeMiddleware];
    const composeArgs = [
        applyMiddleware(...middleware)
    ];
    if (window && window.__REDUX_DEVTOOLS_EXTENSION__) {
        composeArgs.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }
    const store = createStore(
        rootReducer(history),
        preloadedState,
        compose.apply(undefined, composeArgs)
    );
    sagaMiddleware.run(rootSaga);
    return store;
}