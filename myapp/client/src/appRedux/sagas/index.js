//Saga effects
import { all, fork } from 'redux-saga/effects';
import rootSagaAuth from './auth';

export default function* rootSaga() {
  yield all([
    fork(rootSagaAuth)
  ]);
}