import { combineReducers } from 'redux';
import auth from './auth';
import modal from './modal';
import { connectRouter } from 'connected-react-router'

const rootReducer = (history) => combineReducers({
    auth,
    router: connectRouter(history),
    modal
});

export default rootReducer;
