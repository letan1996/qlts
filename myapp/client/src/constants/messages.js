export const KHONG_TIM_THAY_400 = 'Không tìm thấy';
export const KHONG_CO_QUYEN_403 = 'Không có quyền';
export const KHONG_TON_TAI_DV_404 = 'Dịch vụ không tồn tại';
export const DU_LIEU_DANG_SU_DUNG_416 = 'Dữ liệu đã được sử dụng';
export const DU_LIEU_KHONG_TON_TAI_417 = 'Dữ liệu không tồn tại';
export const LOI_HE_THONG_500 = 'Lỗi hệ thống';
export const HE_THONG_KHONG_PHAN_HOI_503 = 'Hệ thống không phản hồi';

