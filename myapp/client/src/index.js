import React from 'react';
import ReactDOM from 'react-dom';
import App from 'src/App';
import { Provider } from 'react-redux';
import configureStore from 'src/appRedux/store';
import { ConnectedRouter } from 'connected-react-router';
import 'src/styles/main.less';
import history from './utils/history';

export const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}> { /* place ConnectedRouter under Provider */}
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
