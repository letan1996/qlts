import userApi from './userApi';
import donViApi from './donViApi';
import phongBanApi from './phongBanApi';
import vaiTroApi from './vaiTroApi';
import permissionApi from './permissionApi';
import duAnApi from './duAnApi';
export {
  userApi,
  donViApi,
  phongBanApi,
  vaiTroApi,
  permissionApi,
  duAnApi
}