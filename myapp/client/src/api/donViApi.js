import { axiosClient } from 'src/utils';
const initUrl = 'api/v1/don-vi';

const getAll = (data) => {
    const requestOptions = {
        method: 'GET',
        url: initUrl,
        ...data
    };
    return axiosClient(requestOptions);
}

const create = (data, cbSuccess, cbError) => {
    const requestOptions = {
        method: 'POST',
        url: initUrl,
        data,
        isShowMessage: true,
        cbSuccess,
        cbError
    };
    return axiosClient(requestOptions)
}

const update = (data, cbSuccess, cbError) => {
    const { _id } = data;
    const requestOptions = {
        method: 'PUT',
        url: `${initUrl}/${_id}`,
        data,
        isShowMessage: true,
        cbSuccess,
        cbError
    };
    return axiosClient(requestOptions);
}

const _delete = (id, cbSuccess, cbError) => {
    const requestOptions = {
        method: 'DELETE',
        url: `${initUrl}/${id}`,
        isShowMessage: true,
        cbSuccess,
        cbError
    };
    return axiosClient(requestOptions);
}
const getDetail = (id) => {
    const requestOptions = {
        method: 'GET',
        url: `${initUrl}/${id}`,
    };
    return axiosClient(requestOptions);
}
const unitApi = {
    create,
    update,
    _delete,
    getAll,
    getDetail,
    initUrl
};
export default unitApi;