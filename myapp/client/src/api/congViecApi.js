import { axiosClient } from 'src/utils';
const initUrl = 'api/v1/cong-viec-ca-nhan';

const get = (params) => {
  const requestOptions = {
    method: 'GET',
    url: initUrl,
    params
  };
  return axiosClient(requestOptions);
}

const create = (data, cbSuccess, cbError) => {
  const requestOptions = {
    method: 'POST',
    url: initUrl,
    data,
    isShowMessage: true
  };
  return axiosClient(requestOptions).then(res => {
    if (res.status === 200 || res.status === 201) {
      cbSuccess && cbSuccess();
    } else {
      cbError && cbError();
    }
    return res;
  });
}

const update = (data, cbSuccess, cbError) => {
  const { _id } = data;
  const requestOptions = {
    method: 'PUT',
    url: `${initUrl}/${_id}`,
    data,
    isShowMessage: true
  };
  return axiosClient(requestOptions).then(
    res => {
      if (res.status === 200 || res.status === 201) {
        cbSuccess && cbSuccess();
      } else {
        cbError && cbError();
      }
      return res
    }
  );
}

const _delete = (id, cbSuccess, cbError) => {
  const requestOptions = {
    method: 'DELETE',
    url: `${initUrl}/${id}`,
    cbSuccess,
    cbError,
    isShowMessage: true
  };
  return axiosClient(requestOptions).then(res => {
    if (res.status === 200 || res.status === 201) {
      cbSuccess && cbSuccess();
    } else {
      cbError && cbError();
    }
    return res;
  });
}

const congViecApi = {
  create,
  update,
  _delete,
  get,
  initUrl
};
export default congViecApi;