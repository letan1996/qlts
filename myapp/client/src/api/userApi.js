import { ADD, EDIT, DELETE, GET } from 'src/constants/apiTypes';
import { axiosClient } from 'src/utils';
const initUrlUser = 'api/v1/user';

const create = (data) => {
  const requestOptions = {
    method: 'POST',
    url: initUrlUser,
    data,
    isShowMessage: true,
    apiType: ADD
  };
  return axiosClient(requestOptions);
}

const update = (data, cbSuccess, cbError) => {
  const options = {
    url: `${initUrlUser}/${data._id}`,
    method: 'PUT',
    data,
    isShowMessage: true,
    apiType: EDIT

  }
  return axiosClient(options, cbSuccess, cbError);
}

const _delete = (id, cbSuccess, cbError) => {
  const requestOptions = {
    method: 'DELETE',
    url: `${initUrlUser}/${id}`,
    cbSuccess,
    cbError,
    isShowMessage: true,
    apiType: DELETE
  };
  return axiosClient(requestOptions).then(res => {
    if (res.status === 200 || res.status === 201) {
      cbSuccess && cbSuccess(res.data);
    } else {
      cbError && cbError();
    }
    return res;
  });
}

const getAll = (data) => {
  const requestOptions = {
    method: 'GET',
    url: initUrlUser,
    params: data,
    apiType: GET
  };
  return axiosClient(requestOptions);
}

const getDetail = (id) => {
  const requestOptions = {
    method: 'GET',
    url: `${initUrlUser}/${id}`,
    apiType: GET
  };
  return axiosClient(requestOptions);
}

const updateToken = (token, cbSuccess, cbError) => {
  const options = {
    url: initUrlUser,
    method: 'put',
    data: { tokenThietBi: token }
  }
  return axiosClient(options).then(res => {
    if (res.status === 200 || res.status === 201) {
      cbSuccess && cbSuccess();
    } else {
      cbError && cbError();
    }
    return res;
  });
}
// function getAll() {
//     const requestOptions = {
//         method: 'GET',
//         headers: authHeader()
//     };

//     return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
// }

// function getById(id) {
//     const requestOptions = {
//         method: 'GET',
//         headers: authHeader()
//     };

//     return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
// }

// function register(user) {
//     const requestOptions = {
//         method: 'POST',
//         headers: { 'Content-Type': 'application/json' },
//         body: JSON.stringify(user)
//     };

//     return fetch(`${config.apiUrl}/users/register`, requestOptions).then(handleResponse);
// }

// function update(user) {
//     const requestOptions = {
//         method: 'PUT',
//         headers: { ...authHeader(), 'Content-Type': 'application/json' },
//         body: JSON.stringify(user)
//     };

//     return fetch(`${config.apiUrl}/users/${user.id}`, requestOptions).then(handleResponse);;
// }

// // prefixed function name with underscore because delete is a reserved word in javascript
// function _delete(id) {
//     const requestOptions = {
//         method: 'DELETE',
//         headers: authHeader()
//     };

//     return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
// }

// function handleResponse(response) {
//     return response.text().then(text => {
//         const data = text && JSON.parse(text);
//         if (!response.ok) {
//             if (response.status === 401) {
//                 // auto logout if 401 response returned from api
//                 logout();
//                 location.reload(true);
//             }

//             const error = (data && data.message) || response.statusText;
//             return Promise.reject(error);
//         }

//         return data;
//     });
// }

const userApi = {
  updateToken,
  update,
  getAll,
  getDetail,
  create,
  _delete
  // register,
  // getAll,
  // getById,
  // update,
  // delete: _delete
};
export default userApi;