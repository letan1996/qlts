import { axiosClient } from 'src/utils';
import { getCookie, setCookie } from 'src/utils';

const auth = {
    /**
    * Logs a user in, returning a promise with `true` when done
    * @param  {string} username The username of the user
    * @param  {string} password The password of the user
    */
    login(username, password) {
        if (auth.loggedIn()) return Promise.resolve(true)

        // Post a fake request
        return axiosClient.post('/api/v1/auth/login', { username, password })
            .then(response => {
                // Save token to local storage

                if (response.status === 200) {
                    setCookie('accessToken', response.data.token);
                    return Promise.resolve(true);
                }
                return Promise.resolve(false);
            })
    },
    /**
    * Logs the current user out
    */
    logout() {
        return axiosClient.post('/logout')
    },
    /**
    * Checks if a user is logged in
    */
    loggedIn() {
        const accessToken = getCookie('accessToken');
        return !!accessToken
    },
    /**
    * Registers a user and then logs them in
    * @param  {string} username The username of the user
    * @param  {string} password The password of the user
    */
    register(username, password) {
        // Post a fake request
        return axiosClient.post('/register', { username, password })
            // Log user in after registering
            .then(() => auth.login(username, password))
    },
    onChange() { }
}

export default auth
