import { Card as CardAntd } from 'antd';
import React from 'react';

export function Card({ children }) {
    return (
        <CardAntd className="custom__card" bordered={false}>
            {children}
        </CardAntd>
    )
}
