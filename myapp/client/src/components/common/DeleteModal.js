import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import React from 'react';
const { confirm } = Modal;

const showDeleteConfirm = (handleOk) => {
    confirm({
        title: 'Thông báo',
        icon: <ExclamationCircleOutlined />,
        content: 'Bạn có chắc chắn muốn xoá',
        okText: 'Đồng ý',
        okType: 'danger',
        cancelText: 'Đóng',
        onOk() {
            handleOk()
        }
    });
}

export default showDeleteConfirm;