import { notification } from 'antd';
import { ADD, EDIT, GET, DELETE } from 'src/constants/apiTypes';
import {
    KHONG_TIM_THAY_400, KHONG_CO_QUYEN_403, KHONG_TON_TAI_DV_404,
    DU_LIEU_KHONG_TON_TAI_417, LOI_HE_THONG_500, HE_THONG_KHONG_PHAN_HOI_503
} from 'src/constants/messages';
const renderStatusError = (status) => {
    let rs;
    switch (status) {
        case 400:
            rs = KHONG_TIM_THAY_400;
            break;
        case 403:
            rs = KHONG_CO_QUYEN_403;
            break;
        case 404:
            rs = KHONG_TON_TAI_DV_404;
            break;
        case 417:
            rs = DU_LIEU_KHONG_TON_TAI_417;
            break;
        case 500:
            rs = LOI_HE_THONG_500;
            break;
        case 503:
            rs = HE_THONG_KHONG_PHAN_HOI_503;
            break;
        default:
            break;
    }
    return rs;
}
export const msgNotification = ({ apiType, status, isError }) => {
    let msg = '';
    let rs = 'thành công';
    console.log(`apiType`, apiType);
    console.log(`status`, status)
    console.log(`isE`, isError)
    switch (apiType) {
        case GET:
            msg = 'Lấy';
            break;
        case ADD:
            msg = 'Thêm';
            break;
        case EDIT:
            msg = 'Cập nhật';
            break;
        case DELETE:
            msg = 'Xoá';
            break;
        default:
            return msg = 'Có lỗi xảy ra'
    }
    if (isError) {
        rs = `thất bại: ${renderStatusError(status)}`;
    }
    return `${msg} dữ liệu ${rs}`;
}
export const openNotification = ({ title, content, apiType, status }) => {
    let config = {
        placement: 'topRight',
        duration: 3,
        message: title,
        description: content,
        status
    }
    switch (true) {
        case status === 200 && apiType !== GET:
            if (apiType) {
                config.description = msgNotification({ apiType });
            }
            notification.success(config);
            break;
        case (400 <= status && status <= 504):
            if (apiType) {
                config.description = msgNotification({ apiType, status, isError: true });
            }
            notification.error(config);
            break;
        default:
            break;
    }
};
