import React from 'react'
import { connect } from 'react-redux'
import ReactModal from 'react-modal'

const mapStateToProps = state => ({
    ...state.modal
})
const ModalContainer = (props) => {
    const { modalProps, hideModal } = props;

    const handleCancel = () => {
        hideModal();
    };

    return (
        <Modal title={title} visible={modalProps.open} onCancel={handleCancel}>
            {props.children}
        </Modal>
    )
}
export default connect(mapStateToProps, null)(ModalContainer)
