import React from 'react'
import { PageHeader as PageHeaderAntd } from 'antd'
export function PageHeader({ title, extra }) {
    return (
        <PageHeaderAntd
            className="custom__pageheader"
            title={title}
            extra={extra}
        />
    )
}
