import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute({ component: Component, loggedIn, ...rest }) {
    return (
        <Route
            {...rest}
            render={(props) => {
                if (!loggedIn) {
                    return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />;
                }
                return <Component {...props} />;
            }}
        />
    );
}

export default PrivateRoute;
