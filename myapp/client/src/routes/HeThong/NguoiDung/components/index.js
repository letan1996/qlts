import React, { useEffect, useState } from 'react';
import { Table, Space, Button, Input } from 'antd';
import { Card, PageHeader } from 'src/components/common';
import { PlusOutlined, RedoOutlined } from '@ant-design/icons';
import { userApi } from 'src/api';
import { convertDataIndex } from 'src/utils/common';
import ModalSubmit from './ModalSubmit';

const { Search } = Input;

const columns = [
    {
        title: 'STT',
        dataIndex: 'key',
        key: 'key',
        width: '5%',
        align: 'center'
    },
    {
        title: 'Ảnh',
        dataIndex: 'anhThe',
        key: 'anhThe',
        width: '12%',
        align: 'center'
    },
    {
        title: 'Username',
        dataIndex: 'username',
        key: 'name',
    },
    {
        title: 'Họ và tên',
        dataIndex: 'fullName',
        key: 'fullName',
    },
    {
        title: 'Hành động',
        key: 'action',
        width: '20%',
        align: 'center',
        render: (text, record) => (
            <Space size="middle">
                <span className="action-table">Chỉnh sửa</span>
                <span className="action-table">Xoá</span>
                <span className="action-table">Đổi mật khẩu</span>
            </Space>
        ),
    },
];
const initFilter = {
    page: 1,
    pageSize: 10,
    keyword: ''
}
const initData = {
    docs: [],
    total: 0
}
export default function NguoiDung() {
    const [data, setData] = useState(initData);
    const { docs, total } = data;

    const [filter, setFilter] = useState(initFilter);
    const { page, pageSize, keyword } = filter;

    const [loadingTable, setloadingTable] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);


    const handleTableChange = (pagination) => {
        const { current } = pagination;
        setFilter(prevState => ({ ...prevState, page: current }));
    }

    const handleFetchData = async (filter) => {
        try {
            setloadingTable(true);
            const res = await userApi.getAll(filter);
            const { docs, totalDocs } = res.data;
            const { page, pageSize } = filter;
            if (res.status === 200) {
                setData({
                    docs: convertDataIndex(docs, page, pageSize),
                    totalDocs
                });
            }
        } finally {
            setloadingTable(false);
        }

    }

    useEffect(() => {
        handleFetchData({ page, pageSize, keyword });
    }, [page, pageSize, keyword])

    const handleReset = () => {
        setFilter(initFilter);
    }

    const handleSearch = (value) => {
        setFilter(prevState => ({ ...prevState, keyword: value }));
    }

    const showModal = () => {
        setIsModalVisible(true);
    }

    const handleAdd = () => {
        showModal();
    }

    return (
        <>
            <ModalSubmit isModalVisible={isModalVisible} setIsModalVisible={setIsModalVisible} />
            <PageHeader title="Người dùng" extra={
                [
                    <Button key="3" icon={<RedoOutlined />} onClick={handleReset}>Tải lại</Button>,
                    <Search
                        key="2"
                        style={{ minWidth: '250px' }}
                        placeholder="Nhập từ khoá tìm kiếm"
                        onSearch={handleSearch}
                        enterButton
                    />,
                    <Button
                        key="1"
                        type="primary"
                        icon={<PlusOutlined />}
                        onClick={showModal}
                    >
                        Thêm mới
                        </Button>
                ]
            } />
            <Card>
                <Table
                    columns={columns}
                    dataSource={docs}
                    onChange={handleTableChange}
                    bordered
                    pagination={{ pageSize, current: page, total }}
                    loading={loadingTable}
                />
            </Card>
        </>
    )
}
