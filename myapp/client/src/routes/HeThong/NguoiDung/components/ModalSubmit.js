import { Button, Form, Input, Modal } from 'antd';
import React, { useState } from 'react';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
}
const ModalSubmit = (props) => {
    const { id, handleEdit, handleAdd, isModalVisible, setIsModalVisible } = props;
    const [form] = Form.useForm();
    const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);

    const onReset = () => {
        form.resetFields();
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        onReset();
    };

    const onSubmit = (values) => {
        if (form.isFieldValidating()) {
            console.log(`values`, values)
            if (id) {
                values.id = id;
                handleEdit(id);
            } else {
                handleAdd(values);
            }
        }
    };

    return (
        <Modal title="Basic Modal" visible={isModalVisible} onCancel={handleCancel}>
            <Form {...layout} form={form} name="control-hooks"
                initialValues={{
                    username: '',
                    password: '',
                    fullName: ''
                }}
            >
                <Form.Item name="username" label="Username" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="password" label="Mật khẩu" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="fullName" label="Họ và tên" >
                    <Input />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit" loading={isLoadingSubmit} onClick={onSubmit}>
                        Lưu
                    </Button>
                    <Button htmlType="button" onClick={onReset}>
                        Reset
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default ModalSubmit;