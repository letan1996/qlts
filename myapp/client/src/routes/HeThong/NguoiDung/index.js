import SignIn from './index';
import { connect } from 'react-redux';
import { hideModal, showModal } from 'src/appRedux/actions/modal';
const mapStateToProps = state => ({
    ...state.modal
});

const mapDispatchToProps = dispatch => ({
    hideModal: () => dispatch(hideModal()),
    showModal: (modalProps, modalType) => {
        dispatch(showModal({ modalProps, modalType }))
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
