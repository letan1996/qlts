import { Route, Switch } from 'react-router-dom';
import React, { Suspense } from 'react';
import { listNavItemHeThong, listNavItemThongKe, listNavItemTrangChu } from 'src/containers/listNav';

const Loading = () => <p>...Loading</p>
const NotFound = () => <p>NotFound</p>

const App = () => {

  const renderRoutes = (data) => {
    return data.map((item, index) => {
      const { Cmp, exact, key } = item;
      return (
        <Route exact={exact} path={key} key={index}>
          {Cmp ? <Cmp /> : null}
        </Route>
      )
    })
  }

  return (
    <div>
      <Suspense fallback={<Loading />}>
        <Switch>
          {renderRoutes(listNavItemTrangChu)}
          {renderRoutes(listNavItemThongKe)}
          {renderRoutes(listNavItemHeThong)}
          <Route component={NotFound} />
        </Switch>
      </Suspense>
    </div>

  );
};

export default App;
