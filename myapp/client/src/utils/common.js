import moment from 'moment';
export const convertISOToDate = (time, type) => {
    if (time) {
        return moment(time).format(type)
    }
    return '';
}
export const convertDataIndex = (arr, page, pageSize) => {
    return arr.map((item, index) => {
        const key = page === 1 ? index + 1 : (page - 1) * pageSize + index + 1;
        return { ...item, key };
    })
}