const splitRoutes = (pathname) => {
    return pathname.split(/\/(them-moi|chinh-sua|chi-tiet)$/)[0];
}
export default splitRoutes;