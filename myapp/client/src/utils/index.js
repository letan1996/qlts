import PrivateRoute from '../guards/PrivateRoute';
import axiosClient from './AxiosClient';
import { getCookie, setCookie, removeCookie } from './Cookies';
import { showAlertErr, showAlertSuccess } from './ShowAlert';
import splitRoutes from './SplitRoutes';
export {
    PrivateRoute,
    axiosClient,
    showAlertErr,
    showAlertSuccess,
    getCookie,
    setCookie,
    removeCookie,
    splitRoutes,
};
