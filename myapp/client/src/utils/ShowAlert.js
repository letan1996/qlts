import { notification } from 'antd';
import React from 'react';
import { InfoCircleOutlined } from '@ant-design/icons';
const showAlertSuccess = (msg) => {
  notification.open({
    message: 'Thông báo',
    duration: 2,
    description: msg,
    icon: <InfoCircleOutlined style={{ color: '#389e0d' }} />
  });
}
const showAlertErr = (msg) => {
  notification.open({
    message: 'Thông báo',
    duration: 2,
    description: msg,
    icon: <InfoCircleOutlined style={{ color: '#cf1322' }} />
  });
}

const showAlertCongViec = () => {
  notification.open({
    message: 'Thông báo',
    duration: 4,
    description: 'Có 1 công việc sắp hết hạn',
    icon: <InfoCircleOutlined style={{ color: '#cf1322' }} />
  });
}

const showAlertNotify = (status, msg) => {
  switch (status) {
    case 200:
    case 201:
      showAlertSuccess('Thành công');
      break;
    case 404:
      showAlertErr('Dịch vụ không tồn tại', msg);
      break;
    case 403:
      showAlertErr('Thất bại', msg);
      break;
    case 500:
      showAlertErr('LỖI HỆ THỐNG', msg);
      break;
    case 'CV':
      showAlertCongViec();
      break;
    default:
      break;
  }
}

export {
  showAlertErr,
  showAlertSuccess,
  showAlertNotify
}
