import Cookies from 'universal-cookie';

const cookies = new Cookies();
export const getCookie = (name) => {
    try {
        return cookies.get(name);
    } catch (error) {
        return undefined;
    }
}
export const setCookie = (name, value) => cookies.set(name, value);
export const removeCookie = (name) => cookies.remove(name);
