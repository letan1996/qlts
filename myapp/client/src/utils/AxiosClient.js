import { API_URL } from 'src/config';
import { getCookie } from './Cookies';
import axios from 'axios';
import { get } from 'lodash';
import { openNotification } from 'src/components/common';

const axiosClient = axios.create({
    baseURL: API_URL,
    validateStatus: () => true,
    headers: {
        'Content-type': 'application/json; charset=utf-8',
    }
});
/**
 *
 * parse error response
 */
function parseError(messages) {
    // error
    if (messages) {
        return messages;
    } else {
        return 'Có lỗi xảy ra';
    }
}
/**
 * parse response
 */
function parseBody(res) {
    const { status, data, config } = res;
    const apiType = get(config, 'apiType', undefined);
    const message = get(data, 'error.message', undefined);
    const options = {
        title: 'Thông báo',
        content: message,
        apiType,
        status
    }
    openNotification(options);
    if (status === 200) {
        const content = get(res, 'data.data', []);
        const rs = { data: content, status };
        return rs;
    } else {
        return {
            isErr: true,
            msg: parseError(message),
            data: []
        };
    }
}

axiosClient.interceptors.response.use((res) => {
    return parseBody(res);
});

axiosClient.interceptors.request.use((config) => {
    const accessToken = getCookie('accessToken');
    config.headers = {
        Authorization: `Bearer ${accessToken}`
    }
    return config;
});

export default axiosClient;